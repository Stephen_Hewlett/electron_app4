// const electron = require('electron')
// const BrowserWindow = electron.BrowserWindow
// const Menu = electron.Menu
// const app = electron.app
const {app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')

let win

function createWindow () {
    win = new BrowserWindow({frame: true, titleBarStyle:'hidden', width: 600, height: 600, minWidth: 400, minHeight: 400})

    win.loadURL(url.format({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file:',
      slashes: true,
  }))


  win.on('closed', () => {
    win = null
  })
    require('./menu/mainmenu')
}

//Here we call the createWindow function when the app has finished loading.

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

//If the app becomes active, but the win variable is empty then the instruction is set to create a new window.

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})